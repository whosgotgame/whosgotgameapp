import React, { Component } from 'react';
import {
    Navigator,
    StyleSheet,
    Text,
    View,
    Image,
    AsyncStorage,
    ActivityIndicator,
    TouchableOpacity,
    Button
} from 'react-native';

import BottomNavigation, { Tab } from 'react-native-material-bottom-navigation'
import Icon from 'react-native-vector-icons/MaterialIcons'
import MainVenueView from './VenueViews/MainVenueView'
import ProfileView from './ProfileViews/ProfileView'
import SocialView from './SocialViews/SocialView'
import AllGamesView from './GamesViews/AllGamesView'
import SportsView from './SportsViews/SportsView'

const styles = require("../../assets/css/styles");

export default class HubView extends Component {

  constructor(props){
    super(props);
    this.state = {
        viewIDG: 2
    }
  }

  componentDidMount(){
    //this.getAPIData();
  }

  gotoView(viewID){
      switch(viewID)
      {
        case 0:
          this.setState({viewIDG: 0});
          break;
        case 1:
          this.setState({viewIDG: 1});
          break;
        case 2:
          this.setState({viewIDG: 2});
          break;
        case 3:
          this.setState({viewIDG: 3});
          break;
        case 4:
          this.setState({viewIDG: 4});
          break;
      }
  }

    render() {
        return(
            <View style={styles.mainBG}>
                {this.renderContent()}
                {this.renderGlobalNav()}
            </View>
        );
    }

    renderGlobalNav()
    {
        return(
            <BottomNavigation
                labelColor="white"
                style={{ height: 56, elevation: 8, position: 'absolute', left: 0, bottom: 0, right: 0 }}
                onTabChange={(newTabIndex) => this.gotoView(newTabIndex)}
                activeTab={this.state.viewIDG}
            >
                <Tab
                    barBackgroundColor="#0F2E63"
                    label="Profile"
                    icon={<Image style={{width: 20, height: 24}} resizeMode={'contain'} source={require('../../assets/images/profile.png')} />}
                />
                <Tab
                    barBackgroundColor="#0F2E63"
                    label="Venues"
                    icon={<Image style={{width: 24, height: 24}} resizeMode={'contain'} source={require('../../assets/images/search.png')} />}
                />
                <Tab
                    barBackgroundColor="#0F2E63"
                    label="Sports"
                    icon={<Image style={{width: 24, height: 24}} resizeMode={'contain'} source={require('../../assets/images/wgglogosmall.png')}/>}
                />
                <Tab
                    barBackgroundColor="#0F2E63"
                    label="Social"
                    icon={<Image style={{width: 24, height: 26}} resizeMode={'contain'} source={require('../../assets/images/chat.png')}/>}
                />
                <Tab
                    barBackgroundColor="#0F2E63"
                    label="Games"
                    icon={<Image style={{width: 24, height: 24}} resizeMode={'contain'} source={require('../../assets/images/fantasy.png')}/>}
                />
            </BottomNavigation>
        );
    }

    renderContent()
    {
        if(this.state.viewIDG == 0)
        {
            return(
                <ProfileView/>
            );
        }
        else if (this.state.viewIDG == 1)
        {
            return(
                <MainVenueView
                    navigator={this.props.navigator}/>
            );
        }
        else if(this.state.viewIDG == 2)
        {
            return(
                <SportsView/>
            );
        }
        else if(this.state.viewIDG == 3)
        {
            return(
                <SocialView/>
            );
        }
        else
        {
            return(
                <AllGamesView/>
            );
        }
    }
}
