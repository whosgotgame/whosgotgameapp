import React, { Component } from 'react';
import {
    Navigator,
    StyleSheet,
    Text,
    ListView,
    View,
    Image,
    AsyncStorage,
    ActivityIndicator,
    TouchableOpacity,
    Button
} from 'react-native';

import VenueListItem from "../../Components/VenueListItem";

const styles = require("../../../assets/css/styles");
const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

var venueData = [];
var loaded = false;

export default class MainVenueView extends Component {

  constructor(props){
    super(props);
    this.state = {
        dataSource: ds.cloneWithRows(venueData),
        results: '',
        data: {}
    };
  }

  componentWillMount(){
    this.getAPIData();
    this.setupData();
  }

  componentDidMount(){
      this.setState({
          dataSource: ds.cloneWithRows(venueData),
      });
  }

  setupData(){
      this.getData();
  }

  async getData(){
      try {
          let value = await AsyncStorage.getItem('data');
          val = JSON.parse(value);
          console.log(val);
          if(val !== null){
              this.setState({
                  data: val
              });
              venueData = [];
              for(var i = 0; i < val.venues.length; i++)
              {
                  var venue = {name:"", address:"", city:"", phone: "", sound: false, sportsnetla: false, hometeam: "No hometeam", totalscreens:0, ontap:0, zip:0, lat:0, long:0, imagelink: ''};
                  venue.name = val.venues[i].venue_name;
                  venue.address = val.venues[i].venue_address;
                  venue.city = val.venues[i].venue_city;
                  venue.zip = val.venues[i].venue_zipcode;
                  venue.lat = val.venues[i].venue_latitude;
                  venue.long = val.venues[i].venue_longitude;
                  venue.phone = val.venues[i].venue_phone;
                  if(val.venues[i].beers_tap != null)
                  {
                      venue.ontap = val.venues[i].beers_tap;
                  }
                  if(val.venues[i].venue_number_screens_total != null)
                  {
                      venue.totalscreens = val.venues[i].venue_number_screens_total;
                  }
                  if(val.venues[i].soundcapability)
                  {
                      venue.sound = true;
                  }
                  else
                  {
                      venue.sound = false;
                  }
                  if(val.venues[i].venue_teams != null)
                  {
                      venue.hometeam = val.venues[i].venue_teams + " is popular";
                  }
                  if(val.venues[i].venueimage.venueimage.url != null)
                  {
                      venue.imagelink = 'beta.we-game.com' + val.venues[i].venueimage.venueimage.url;
                  }
                  else
                  {
                      venue.imagelink = '../../assets/images/placeholderimg.jpg';
                  }
                  if(val.venues[i].sportsnetla)
                  {
                      venue.sportsnetla = true;
                  }
                  else
                  {
                        venue.sportsnetla = false;
                  }
                  venueData.push(venue);
              }
              this.setState({
                  dataSource: ds.cloneWithRows(venueData)
              });
              loaded = true;
              console.log("THE VENUE DATA IS");
              console.log(venueData);
          }
      } catch (e) {
          console.log(e);
      }
  }

  getAPIData(){
    return fetch("http://beta.we-game.com/api/v1/venues")
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          results: responseJson
        });
        this.storeData();
        this.setState({
          done: true
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  storeData(){
      let data = this.state.results;
      AsyncStorage.setItem('data', JSON.stringify(data));
  }

  gotoDescription(rowData){
      this.props.navigator.push({
          id: 'VenueProfile',
          name: rowData.venue_name,
          rowDat: rowData,
      });
  }


  render() {
    return (
      <View style={styles.mainBG}>
      <View style={{flex: .25}}>
      </View>
      <ListView
          style={styles.locations}
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
              <View style={{backgroundColor: 'white'}}>
                  <TouchableOpacity style={styles.wrapper} onPress={this.gotoDescription.bind(this, rowData)}>
                        <VenueListItem rowData={rowData}/>
                  </TouchableOpacity>
                  <View style={styles.separator} />
              </View>
      }/>
      </View>
    );
  }
}

module.exports = MainVenueView;
