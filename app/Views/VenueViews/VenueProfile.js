import React, { Component } from 'react';
import { Button, StyleSheet, ScrollView, Image, BackAndroid, Text, View, Navigator, TouchableHighlight, TouchableOpacity, AsyncStorage, Dimensions } from 'react-native';
import MapView from 'react-native-maps';
import call from 'react-native-phone-call';

const styles = require( "../../../assets/css/styles");
const stylesa = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: 400,
    width: 400,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    marginTop: 1,
    width: devWidth,
    height: 500
  },
});

let {devHeight, devWidth} = Dimensions.get('window');
let mapLongitude, mapLatitude = 0;
let mRegion = {
    latitude: 34.0818,
    longitude: -118.41339,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
};
class VenueProfile extends Component
 {
     static NavigationBarRouteMapper = props => ({
         LeftButton(route, navigator, index, navState) {
             return (
               <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}
                   onPress={() => navigator.parentNavigator.pop()}>
                 <Text style={{color: 'white', margin: 10}}>
                   Back
                 </Text>
               </TouchableOpacity>
             );
         },
         RightButton(route, navigator, index, navState) {
             return null;
         },
         Title(route, navigator, index, navState) {
             return (
               <TouchableOpacity style={{flex: 1, justifyContent: 'center'}}>
                 <Text style={{color: 'white', margin: 10, marginLeft: 50, fontSize: 24, fontWeight: 'bold'}}>
                    {props.data.name}
                 </Text>
               </TouchableOpacity>
             );
         },
     })

     constructor(props){
         if(!props.data.long)
         {
             mapLatitude = 34.0818;
         }
         else
         {
             mapLatitude = props.data.long;
         }
         if(!props.data.lat)
         {
            mapLongitude = -118.41339;
         }
         else
         {
              mapLongitude = props.data.lat;

         }
       mRegion = {
           latitude: parseFloat(mapLatitude),
           longitude: parseFloat(mapLongitude),
           latitudeDelta: 0.0922,
           longitudeDelta: 0.0421,
       }
       console.log(mapLatitude);

       super(props);
     }

     callPush(){
         let pNumber = this.props.data.phone;
         pNumber.replace(/[^0-9.]/g, "");
         console.log(pNumber);
         let args = {
            number: pNumber,
            prompt: true
         };

        call(args).catch(console.error);
     }

     barpayPush(){

     }

     uberPush(){

     }

     render()
     {
         console.log(this.props.data);
         return (
             <Navigator
                 renderScene={this.renderScene.bind(this)}
                 navigator={this.props.navigator}
                 navigationBar={
                   <Navigator.NavigationBar style={{backgroundColor: '#18395E'}}
                       routeMapper={VenueProfile.NavigationBarRouteMapper(this.props)} />
                 } />
        );
    }
    renderScene(route, navigator) {
            return (
                <ScrollView style={{flex: 1}}>
                    <Image source={{uri: 'http://beta.we-game.com/uploads/venue/venueimage/405/348s.jpg'}} style={{width: 450, height: 200, marginTop: 50}} />
                    <View style={{width: devWidth, height: 1, backgroundColor: 'grey'}}/>
                    <View style={{flex: .5, backgroundColor: '#ffffff'}}>
                        <Text style={{alignItems: 'flex-start', marginLeft: 10, marginTop: 5}}><Text style={{alignSelf: 'flex-start'}}><Text style={{alignSelf: 'flex-start', fontSize: 18}}>{this.props.data.name}</Text> - {this.props.data.city}</Text> xxx Miles</Text>
                        <Text style={{marginLeft: 10, marginBottom: 5}}> {this.props.data.totalscreens} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../../assets/images/TVIcon.png')}/> {this.props.data.ontap} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../../assets/images/BeerIcon.png')}/></Text>
                        <View style={{width: devWidth, height: 2, backgroundColor: 'grey', marginTop: 0}}/>
                        <Text style={{marginTop: 2}}>{this.props.data.address}, {this.props.data.city}, {this.props.data.zip}</Text>
                    </View>
                    <View style={{height: 300, flex: 2, backgroundColor: 'grey'}}>
                        <View style={{width: devWidth, height: 1, backgroundColor: 'grey'}}/>
                        <Text>{JSON.stringify(this.props.data)}</Text>
                        <MapView
                            style={stylesa.map}
                          region={mRegion}
                        >
                            <MapView.Marker
                                coordinate={mRegion}
                                image={require('../../../assets/images/WGGIcon.png')}
                                title={this.props.data.name}
                                description={this.props.data.address}/>
                        </MapView>
                    </View>
                    <View style={{width: devWidth, height: 1, backgroundColor: 'grey'}}/>
                    <View style={{flexDirection: 'row'}} >
                        <View style={{width: 1, backgroundColor: 'grey'}}/>
                        <TouchableOpacity onPress={this.callPush.bind(this)} style={{width: Dimensions.get('window').width / 3, height: 50, backgroundColor: 'white'}}>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text>Call</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{width: 1, backgroundColor: 'grey'}}/>
                        <TouchableOpacity style={{width: Dimensions.get('window').width / 3, height: 50, backgroundColor: 'white'}}>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text>Check-in/Claim</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{width: 1, backgroundColor: 'grey'}}/>
                        <TouchableOpacity style={{width: Dimensions.get('window').width / 3, height: 50, backgroundColor: 'white'}}>
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <Text>Call an Uber</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{width: 1, backgroundColor: 'grey'}}/>
                    </View>
                    <View style={{width: devWidth, height: 1, backgroundColor: 'grey'}}/>
                </ScrollView>
            );
    }
}

module.exports = VenueProfile;
