import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    Image,
} from 'react-native';

const styles = require( "../../assets/css/styles");

export default class VenueListItem extends Component {

    render() {
        if(this.props.rowData.sound && this.props.rowData.sportsnetla)
        {
            return (
                <View style={styles.wrapper}>
                    <View style={{flex:1, backgroundColor: 'white'}}>
                        <Image source={require('../../assets/images/placeholderimg.png')} style={{width: 140, height: 108}} />
                    </View>
                    <View style={{flex:2, backgroundColor: 'white', paddingLeft: 6}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                            {this.props.rowData.name}
                        </Text>
                        <Text>
                            {this.props.rowData.address}, {this.props.rowData.city}     Distance
                        </Text>
                        <Text>
                            {this.props.rowData.hometeam}
                        </Text>
                        <Text>
                            {this.props.rowData.ontap} <Image source={require('../../assets/images/BeerIcon.png')}/>
                            {this.props.rowData.totalscreens} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/TVIcon.png')}/>
                            <Text>   </Text>
                            <Image source={require('../../assets/images/VenueSound.png')}/>
                            <Text>   </Text>
                            <Image style={{width: 70, height: 70, paddingLeft: 100}} source={require('../../assets/images/Dodgers.png')}/>
                       </Text>
                        <Text>
                        </Text>
                    </View>
                </View>
            );
        }
        else if(this.props.rowData.sound)
        {
            return (
                <View style={styles.wrapper}>
                    <View style={{flex:1, backgroundColor: 'white'}}>
                        <Image source={require('../../assets/images/placeholderimg.png')} style={{width: 140, height: 108}} />
                    </View>
                    <View style={{flex:2, backgroundColor: 'white', paddingLeft: 6}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                            {this.props.rowData.name}
                        </Text>
                        <Text>
                            {this.props.rowData.address}, {this.props.rowData.city}     Distance
                        </Text>
                        <Text>
                            {this.props.rowData.hometeam}
                        </Text>
                        <Text style={{marginBottom: 10}}>
                            {this.props.rowData.ontap} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/BeerIcon.png')}/>
                            {this.props.rowData.totalscreens} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/TVIcon.png')}/>
                            <Text>   </Text>
                            <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/VenueSound.png')} />
                        </Text>
                    </View>
                </View>
            );
        }
        else if(this.props.rowData.sportsnetla)
        {
            return (
                <View style={styles.wrapper}>
                    <View style={{flex:1, backgroundColor: 'white'}}>
                        <Image source={require('../../assets/images/placeholderimg.png')} style={{width: 140, height: 108}} />
                    </View>
                    <View style={{flex:2, backgroundColor: 'white', paddingLeft: 6}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                            {this.props.rowData.name}
                        </Text>
                        <Text>
                            {this.props.rowData.address}, {this.props.rowData.city}     Distance
                        </Text>
                        <Text>
                            {this.props.rowData.hometeam}
                        </Text>
                        <Text style={{marginBottom: 10}}>
                            {this.props.rowData.ontap} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/BeerIcon.png')}/>
                            {this.props.rowData.totalscreens} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/TVIcon.png')}/>
                            <Text>   </Text>
                            <Image style={{width: 70, height: 70, paddingLeft: 100}} source={require('../../assets/images/Dodgers.png')}/>
                        </Text>
                    </View>
                </View>
            );
        }
        else
        {
            return (
                <View style={styles.wrapper}>
                    <View style={{flex:1, backgroundColor: 'white'}}>
                        <Image source={require('../../assets/images/placeholderimg.png')} style={{width: 140, height: 108}} />
                    </View>
                    <View style={{flex:2, backgroundColor: 'white', paddingLeft: 6}}>
                        <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                            {this.props.rowData.name}
                        </Text>
                        <Text>
                            {this.props.rowData.address}, {this.props.rowData.city}     Distance
                        </Text>
                        <Text>
                            {this.props.rowData.hometeam}
                        </Text>
                        <Text style={{fontSize: 14, marginBottom: 10}}>
                             {this.props.rowData.ontap} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/BeerIcon.png')}/>
                             {this.props.rowData.totalscreens} <Image style={{width: 70, height: 70}} resizeMode={'contain'} source={require('../../assets/images/TVIcon.png')}/>
                        </Text>
                    </View>
                </View>
            );
        }
    }
}

module.exports = VenueListItem;
