import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Navigator,
} from 'react-native';
import LoginView from './Views/LoginView';
import HubView from './Views/HubView';
import VenueProfile from './Views/VenueViews/VenueProfile'

const styles = require( "../assets/css/styles");

export default class App extends Component {

    render() {
        return (
            <Navigator
                initialRoute={{id: 'Hub', name: 'HubView'}}
                renderScene={this.renderScene1.bind(this)}
                configureScene={(route) => {
                    if(route.sceneConfig) {
                        return route.sceneConfig;
                    }
                    return Navigator.SceneConfigs.FloatFromRight;
                }}
            />
        );
    }

    renderScene1(route, navigator) {
        var routeID = route.id;
        console.log("Route ID is: " + route.id);
        if(routeID === 'Hub') {
            return(
                <HubView
                    navigator={navigator}/>
            );
        }
        else if(routeID === "VenueProfile") {
            return(
                <VenueProfile
                    navigator={navigator}
                    data={route.rowDat} />
            );
        }
    }
}
