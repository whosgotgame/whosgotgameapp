const React = require("react-native");
import {
    StyleSheet,
    Dimensions
} from "react-native";

var {devHeight, devWidth} = Dimensions.get('window');

module.exports = StyleSheet.create({
    mainBG: {
        width: devWidth,
        height: devHeight,
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    wrapper: {
        flexWrap: 'wrap',
        alignItems: 'flex-start',
        flexDirection:'row'
    },
    locations:{
        width: devWidth - 40
    },
    separator: {
        height: 3,
        backgroundColor: '#dddddd'
    },
    imageHeader: {
        width: devWidth,
        height: 200,
        paddingTop: 20,
    },
});
